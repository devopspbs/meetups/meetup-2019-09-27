<!-- .slide: data-background-image="images/NIGHTS-MEETUP.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## O Que Temos Para Hoje

* Visão Sistêmica - Thiago Almeida
* Agil e Scrum - Ana Sousa
* Personal Branding - Dhony Silva

----  ----

## Recado

https://startupweekend.org/

----  ----

## O que é Startup Weekend?

[![O que é Techstars Startup Weekend?](images/o-que-e-techstars-startup-weekend.png)](https://www.youtube.com/watch?v=5lm6g720-r0 "O que é Techstars Startup Weekend?")

----  ----

<!-- .slide: data-background-image="images/Techstars-Startup-Weekend-Academy-Campeche-2018-el-fin-de-semana-ideal-para-tu-emprendimiento2.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

<a href="https://www.meetup.com/devopspbs/"><img width="240" src="images/DEVOLPBS.png" alt="DevOpsPBS"></a>

Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc)

----  ----

## Conduta

"Participe de maneira autêntica e ativa. Ao fazer isso, você contribui para a saúde e a longevidade dessa comunidade."

[//]: # (https://garoa.net.br/wiki/C%C3%B3digo_de_Conduta_Completo)

----  ----

## Site e Mídias Sociais

* **Site oficial:** [devopspbs.org](https://devopspbs.org)

* **Mídias sociais:**
  - [meetup.com/devopspbs](https://www.meetup.com/devopspbs/)
  - [gitlab.com/devopspbs](https://gitlab.com/devopspbs)
  - [instagram/devopspbs](https://www.instagram.com/devopspbs/)

* **Grupos de discussão:**
  - Grupo [DevOpsPBS](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ) no Telegram
  - Grupo [TECNOLOGIA PBS](https://chat.whatsapp.com/7XfTiu53icSKKbANTQnMGH) no Whatsapp

----  ----

## Apoio:

<a href="http://www.parauapebas.pa.gov.br/"><img height="200" src="images/parauapebas.svg" alt="Parauapebas"></a>

----  ----

## Dica: Linkedin Mobile

![](images/in.png)
