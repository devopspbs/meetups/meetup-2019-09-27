# Meetup Setembro 2019

## Palestras

[Abertura](https://devopspbs.gitlab.io/meetups/meetup-2019-09-27/)

Visão Sistêmica - Thiago Almeida

Agil e Scrum - Ana Sousa, Analista de Sistemas

Personal Branding - Dhony Silva


> As apresentações estão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
